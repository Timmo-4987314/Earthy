# EARTHY: Group 5

By
- _Timmo de Haas		    4987314_
- _Jorrit Parmetier		4939905_
- _Daniel van der Helm	    4160398_
- _Isidoros Spanolios	    4846982_
- _Lama Idrees		        5137330_


## Content

This folder contains the documents for the first phase of the project: Configuating.
The following deliverables can be found in this folder:
- The main design problems
- A general flowchart for all phases, and a detailed one for the first phase
- A programme of Requirements
- A REL-chart
- A Bubble Diagram



