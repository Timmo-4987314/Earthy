# EARTHY: Group 5

By
- _Timmo de Haas		    4987314_
- _Jorrit Parmetier		4939905_
- _Daniel van der Helm	    4160398_
- _Isidoros Spanolios	    4846982_
- _Lama Idrees		        5137330_


## Content

This folder contains the documents for the second phase of the project: Shaping.
The following deliverables can be found in this folder:
- A low-polygon mesh delineating the basic shape of the current building with the tessellation of the roofs
- A markdown page containing information about the process that lead to the building design

The project files can be downloaded for review via [this link](https://drive.google.com/drive/folders/16WYK3Eqz3NSbp6iTuLk7dtknFirWh9J2?usp=sharing)

Content:
- Mid term presentation pdf
- Basic mesh of the project

## Project summary
We choose to build a music center because we believe that:
> “music is a language that everyone can understand, it can connect people from different cultures and let people forget their problems for a moment”


## What's next
The next phase of the project is Structuring phase.
Based on the feedback given after the mid-term presentation the following items will be researched going into this phase:

- Flowchart review
The flowchart will be enhanced and created with more detail to provide an organized and logical path for the decision-making progress.

-  Restructure of the floorplan
The current floorplan will be changed based on a research of similar programs and based on a analysis of the transition between public and private bases through the means of a metro diagram.
-  Research earthy domes
Going into the structuring phase it is important to focus more on how to construct the current design. Therefore a research will be carried out focusing on dome sizes using only earthy materials. 




